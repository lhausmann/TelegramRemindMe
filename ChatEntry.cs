﻿using System.Collections.Generic;

namespace TelegramRemindMe
{
    public class ChatEntry
    {
        public long ChatId { get; }
        public List<TaskEntry> _tasklist { get; set; } = new List<TaskEntry>();
        public ChatStatusEnum status { get; set; } = ChatStatusEnum.Idle;
        public bool IsWorkingMessage { get; set; } = false;
        public int WorkingMsg { get; set; } = 0;
        public bool AllTasksShown { get; set; } = false;
        
        public ChatEntry(long chatId)
        {
            ChatId = chatId;
        }
    }

    public enum ChatStatusEnum
    {
        Idle,
        WaitingTaskTitle,
        WaitingTaskComplete
    }

    public class TaskEntry
    {
        public bool Completed { get; set; } = false;
        public string Title { get; }
        
        public TaskEntry(string title)
        {
            Title = title;
        }
    }
}